<?php

use model\Route;

Route::get($url . '/index', 'ProductController', 'Index');
Route::get($url . '/create', 'ProductController', 'Create');
Route::post($url . '/store', 'ProductController', 'Store');
Route::post($url . '/delete', 'ProductController', 'Delete');
