<?php

use model\Database;
use model\Route;

spl_autoload_register(function ($className) {
    include_once($className . ".php");
});
$path = $_SERVER['REQUEST_URI'];
$url = '/scandiweb';

require('Router/Route.php');
Route::run($path);
